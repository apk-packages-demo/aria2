# [aria2](https://pkgs.alpinelinux.org/packages?name=aria2)

Download utility for HTTP(S), (S)FTP, Bittorrent, and Metalink

(Unofficial demo and howto)

## See also
* [Torrent client for the command-line? \[duplicate\]](https://askubuntu.com/questions/29872/torrent-client-for-the-command-line)
